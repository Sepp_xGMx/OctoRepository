﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MapEditor
{
    /// <summary>
    /// Liste der Werkzeuge im Editor
    /// </summary>
    internal enum ToolType
    {
        CellTypeGras,
        CellTypeSand,
        CellTypeWater,
        ItemDelete,
        ItemTree
    }
}
